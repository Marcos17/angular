import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects'
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators'
import { DestinoViaje } from './destino-viaje .model';
import { HttpClientModule } from '@angular/common/http';


// ESTADO
//estado global de la app
export interface DestinosViajesState{
    items: DestinoViaje[];
    loading: boolean;
    favorito: DestinoViaje;
}

export function initializeDestinosViajesState(){
    return {
        items: [],
        loading: false,
        favorito: null
    };
};

// ACCIONES
// eventos que modifican el estado de la app
export enum DestinosViajesActionTypes { // aqui se definenen los string de todos los estados del sistema
    NUEVO_DESTINO = '[Destinos Viajes] Nuevo',
    ELEGIDO_FAVORITO = '[Destinos Viajes] Favorito',
    VOTE_UP = '[Destinos Viajes] Vote Up',  
    VOTE_DOWN = '[Destinos Viajes] Vote Down',
    VOTE_RESET = '[Destinos Viajes] Vote Reset' , 
    INIT_MY_DATA = '[Destinos Viajes] Init My Data'
}

export class NuevoDestinoAction implements Action{
    type = DestinosViajesActionTypes.NUEVO_DESTINO;
    constructor(public destino: DestinoViaje){}
}

export class ElegidoFavoritoAction implements Action{
    type = DestinosViajesActionTypes.ELEGIDO_FAVORITO;
    constructor(public destino: DestinoViaje){}
}

export class VoteUpAction implements Action{
    type = DestinosViajesActionTypes.VOTE_UP;
    constructor(public destino: DestinoViaje){}
}

export class VoteDownAction implements Action{
    type = DestinosViajesActionTypes.VOTE_DOWN;
    constructor(public destino: DestinoViaje){}
}

export class VoteResetAction implements Action{
    type = DestinosViajesActionTypes.VOTE_RESET;
    constructor(public destino: DestinoViaje){}
}

export class initMyDataAction implements Action{
    type = DestinosViajesActionTypes.INIT_MY_DATA;
    constructor(public destinos: string[]){}
}

export type DestinosViajesActions = NuevoDestinoAction | ElegidoFavoritoAction | VoteDownAction | VoteUpAction | VoteResetAction | initMyDataAction; //Una variable que unifica todos los tipos de datos que son acciones sobre destinosViajes 

// REDUCERS
//es un proceso donde se reciben las acciones y se llevan a cabo. 
//El reducer primero recibe el estado actual con *state* y luego el cambio que se va a realizar con *action*.
//Luego con un switch se evalua cual es el cambio a realizar, sino coincide con niguno de los parametros especificados en el switch, se devuelve el estado original
export function reducerDestinosViajes(
    state: DestinosViajesState,
    action: DestinosViajesActions
): DestinosViajesState{
    switch (action.type){
        case DestinosViajesActionTypes.NUEVO_DESTINO: {
            return {
                ...state,
                items: [...state.items, (action as NuevoDestinoAction).destino]
            };
        };
        case DestinosViajesActionTypes.ELEGIDO_FAVORITO: {
            state.items.forEach( x => x.setSelected(false));
            let fav:DestinoViaje = ( action as ElegidoFavoritoAction).destino;
            fav.setSelected(true);
            return {
                ... state,
                favorito: fav
            };
        }
        case DestinosViajesActionTypes.VOTE_UP: {
           const d: DestinoViaje = (action as VoteUpAction).destino;
            d.voteUp();
            return {... state
            };
        }
        case DestinosViajesActionTypes.VOTE_DOWN: {
           const d: DestinoViaje = (action as VoteUpAction).destino;
            d.voteDown();
            return {... state};
        }
        case DestinosViajesActionTypes.VOTE_RESET: {
           const d: DestinoViaje = (action as VoteResetAction).destino;
            d.voteReset();
            return {... state};
        }
        case DestinosViajesActionTypes.INIT_MY_DATA: {
            const destinos: string[] = (action as initMyDataAction).destinos;
            return{
                ...state,
                items: destinos.map( (d) => new DestinoViaje(d, '') )
            }
        }   
    }
    return state;
}

// EFFECTS
//se usan para activar una accion en funcion de otra, no pora alterar el estado de la app.
@Injectable()
export class DestinosViajesEffects {
    @Effect() 
    nuevoAgregado$: Observable<Action> = this.actions$.pipe(
        ofType(DestinosViajesActionTypes.NUEVO_DESTINO),
        map((action:NuevoDestinoAction) => new ElegidoFavoritoAction(action.destino))
    )

    constructor(private actions$: Actions){}
}