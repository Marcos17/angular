import{v4 as uuid} from 'uuid';

export class DestinoViaje {
    private selected: boolean;
    public servicios:string[];
    id=uuid();
    
    constructor(public nombre: string, public imagenUrl: string, public votes:number = 0){
        this.servicios = ['pileta', 'desayuno'];
    }
    
    isSelected(): Boolean {
            return this.selected; //indica si es true o false
        }
    setSelected(s: boolean){
        this.selected = s; // recibe la indicacion para marcarlo o desmarcarlo
    }
    voteUp(){
       this.votes++;
    }
    voteDown(){
       this.votes--;
    }
    voteReset(){
       this.votes = 0;
    }
    
}