import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, Injectable, InjectionToken, NgModule } from '@angular/core';
import { StoreModule as NgRxStoreModule, ActionReducerMap, StoreModule, Store } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DestinoViajeComponent } from './components/destino-viaje/destino-viaje.component';
import { ListaDestinosComponent } from './components/lista-destinos/lista-destinos.component';
import { DestinoDetalleComponent } from './components/destino-detalle/destino-detalle.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FromDestinoViajeComponent } from './components/from-destino-viaje/from-destino-viaje.component';
import { DestinosViajesState, reducerDestinosViajes, initializeDestinosViajesState, DestinosViajesEffects, initMyDataAction } from './models/destinos-viajes-states.model';
import { LoginComponent } from './components/login/login/login.component';
import { ProtectedComponent } from './components/protected/protected/protected.component';
import { AuthService } from './services/auth.service';
import { UsuarioLogueadoGuard } from './guards/usuario-logueado/usuario-logueado.guard';
import { VuelosComponentComponent } from './components/vuelos/vuelos-component/vuelos-component.component';
import { VuelosMainComponentComponent } from './components/vuelos/vuelos-main-component/vuelos-main-component.component';
import { VuelosMasInfoComponentComponent } from './components/vuelos/vuelos-mas-info-component/vuelos-mas-info-component.component';
import { VuelosDetalleComponentComponent } from './components/vuelos/vuelos-detalle-component/vuelos-detalle-component.component';
import { ReservasModule } from './reservas/reservas.module';
import { HttpClient, HttpClientModule, HttpHeaders, HttpRequest } from '@angular/common/http';
import Dexie from 'dexie';
import { DestinoViaje } from './models/destino-viaje .model';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { from, Observable } from 'rxjs';
import { flatMap } from 'rxjs/operators';
import { NgxMapboxGLModule } from 'ngx-mapbox-gl';

  //app config
export interface AppConfig{
  apiEndpoint: string;
}
const APP_CONFIG_VALUE: AppConfig = {
  apiEndpoint: 'http://localhost:3000'
}
export const APP_CONFIG = new InjectionToken<AppConfig>('app.config');
  // fin app config

//redux init (el estado global de la app)
export interface AppState {
  destinos: DestinosViajesState
};
//los reducers que llevan acabo los cambios
const reducers: ActionReducerMap<AppState>={
  destinos: reducerDestinosViajes
};
// INICIALIZACION
let reducersInitialState = {
  destinos: initializeDestinosViajesState()
}

//api init
export function init_app(appLoadService: AppLoadService): () => Promise<any> {
  return () => appLoadService.initializeDestinosViajesState();
}

@Injectable()
class AppLoadService{
  constructor(private store: Store<AppState>, private http: HttpClient){}
  async initializeDestinosViajesState(): Promise<any> {
    const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token-seguridad'});
    const req = new HttpRequest('GET', APP_CONFIG_VALUE.apiEndpoint + '/my', {headers: headers} );
    const response: any = await this.http.request(req).toPromise();
    this.store.dispatch(new initMyDataAction(response.body));
  }
}
//FIN api init

// DEXIE db
export class Translation {
  constructor(public id: number, public lang: string, public key: string, public value: string){}
}

@Injectable({  providedIn: 'root' })
export class MyDataBase extends Dexie {
  destinos: Dexie.Table<DestinoViaje, number>;
  translations: Dexie.Table<Translation,number>
  constructor( ){
    super('MyDataBase');
    this.version(1).stores({
      destinos: '++id, nombre, imagenUrl',
    });
    this.version(2).stores({
      destinos: '++id, nombre, imagenUrl',
      translations: '++id, lang, key, value'
    });
  }
}

export const db = new MyDataBase();

//Fin dexie db

//i18n ini
@Injectable()
class TranslationLoader implements TranslateLoader{
    constructor(private http: HttpClient){}

    getTranslation(lang: string): Observable<any> {
      const promise = db.translations
        .where('lang') //si este lenguaje que nos pasan...
        .equals(lang)// ..es igual a este
        .toArray()// lo pasa como array
        .then(results => {
              if(results.length === 0){
                return this.http
                  .get<Translation[]>(APP_CONFIG_VALUE.apiEndpoint + '/api/translation?lang=' + lang)
                  .toPromise()
                  .then(apiResults => {
                    db.translations.bulkAdd(apiResults);//esto almacena el resultado de la promesa en la base de datos.
                    return apiResults; // y aca muestra el resultado de la traduccion
                  });
              }
              return results; // esto solo sucede si se encontro una traduccion ya existente a nivel local
            }).then( (traducciones) => {
              console.log('traducciones cargadas: ');
              console.log(traducciones);
              return traducciones;
            }).then( (traducciones) => {
              return traducciones.map( (t) => ({ [t.key]: t.lang}) );
            });
      return from(promise).pipe(flatMap((elems) => from(elems)));
    }
    
  }
function HttpLoaderFactory( http: HttpClient) {
  return new TranslationLoader(http);
}
//fin i18n ini


@NgModule({
  declarations: [
    AppComponent,
    DestinoViajeComponent,
    ListaDestinosComponent,
    DestinoDetalleComponent,
    FromDestinoViajeComponent,
    LoginComponent,
    ProtectedComponent,
    VuelosComponentComponent,
    VuelosMainComponentComponent,
    VuelosMasInfoComponentComponent,
    VuelosDetalleComponentComponent,
    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    NgRxStoreModule.forRoot(reducers, {initialState: reducersInitialState,
      runtimeChecks:{

        strictStateImmutability: false,
        
        strictActionImmutability: false,
        
        }
    }),
    EffectsModule.forRoot([DestinosViajesEffects]),
    StoreDevtoolsModule.instrument(),
    ReservasModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (HttpLoaderFactory),
        deps: [HttpClient]
      }
    }),
    NgxMapboxGLModule
  ],
  providers: [
    AuthService,
    UsuarioLogueadoGuard,
    {provide: APP_CONFIG, useValue:APP_CONFIG_VALUE},
    AppLoadService,
    //APP_INITIALIZER es una injectableToken provisionado por angular que se activa cuando inicia
    //la aplicacion y ejecuta las tareas que se le indican
    //MULTI : TRUE, signigica que se pueden utilizar mas de un APP-INITIALIZER para ejecutar distintas tareas, en un mismo modulo o en varios.
    {provide: APP_INITIALIZER, useFactory: init_app, deps: [AppLoadService], multi: true },
    MyDataBase,
   
   
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
