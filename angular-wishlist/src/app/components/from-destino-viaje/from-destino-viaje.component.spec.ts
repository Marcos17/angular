import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FromDestinoViajeComponent } from './from-destino-viaje.component';

describe('FromDestinoViajeComponent', () => {
  let component: FromDestinoViajeComponent;
  let fixture: ComponentFixture<FromDestinoViajeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FromDestinoViajeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FromDestinoViajeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
