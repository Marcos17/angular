import { Component, EventEmitter, forwardRef, Inject, OnInit, Output } from '@angular/core';
import { DestinoViaje } from '../../models/destino-viaje .model';
import {FormGroup, FormBuilder, Validators, FormControl, ValidatorFn} from '@angular/forms'
import { fromEvent } from 'rxjs';
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { ajax, AjaxResponse } from 'rxjs/ajax';
import { APP_CONFIG, AppConfig } from '../../app.module';


@Component({
  selector: 'app-from-destino-viaje',
  templateUrl: './from-destino-viaje.component.html',
  styleUrls: ['./from-destino-viaje.component.css']
})
export class FromDestinoViajeComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  fg:FormGroup;
  minLongitud = 1;
  searchResults: string[];

  //formbuilder es el responsable de crear el formgroup
  constructor(fb: FormBuilder, @Inject(forwardRef( () => APP_CONFIG ) ) private config: AppConfig) { 
    this.onItemAdded = new EventEmitter();

    this.fg = fb.group({  //formgroup es el formulario en si, ya construido
      nombre:['', Validators.compose([
        Validators.required,
        this.nameValidatorParametrizable(this.minLongitud)
      ])],//form controls
      url:['',Validators.required]//form controls
    });

    //solo para mostrar en consola los cambios del form
    this.fg.valueChanges.subscribe((form: any) => {
      console.log('cambio el formulario:', form)
    });
  }

  ngOnInit(){
    let elemNombre = <HTMLInputElement>document.getElementById('nombre');
    fromEvent(elemNombre, 'input')//Observable de eventos sobre el input
      .pipe(//una seguidilla de operaciones a realizar sobre el string elemNombre
        map((e:KeyboardEvent)=>(e.target as HTMLInputElement).value),//extrae la cadena completa de string luego de presionar una tecla
        filter(text => text.length > 1), // si la cadena tiene mas de dos caracteres continuan las operaciones
        debounceTime(200),// va a retornar lo ultimo que se escribio pasados los 200milisec, mientras se reciba informacion antes de ese tiempo va a quedar en espera
        distinctUntilChanged(),//se mantiene en standby hasta que recibe un dato distinto al anterior, por ejemplo cuando excriben una letra y la borran antes de los 200mlsec, la palabra va a seguir siendo lo misma
        switchMap((text:string) => ajax(this.config.apiEndpoint + '/ciudades?q=' + text ))
      ).subscribe(ajaxResponse => this.searchResults = ajaxResponse.response);  
  }

  guardar(nombre:string, url:string):boolean{
    const d = new DestinoViaje(nombre, url);
    this.onItemAdded.emit(d);
    return false;
  }

  nameValidator(control: FormControl): { [s:string]:boolean} {
    let l = control.value.toString().trim().length; //trim elimina los espacios en blanco
    if(l>0 && l<5){
      return {invalidName:true}
    }
    return null;
  }

  nameValidatorParametrizable(minLong: number): ValidatorFn{ // validatorFn corresponde a Angular-Forms
    return (control: FormControl):{[s:string]:boolean} | null => {
      let l = control.value.toString().trim().length; //trim elimina los espacios en blanco
    if(l>0 && l<minLong){
      return {minLongName:true}
    }
    }
  }
}
