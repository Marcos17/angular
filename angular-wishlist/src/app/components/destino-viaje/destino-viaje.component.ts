import { Component, OnInit, Input, HostBinding, EventEmitter, Output } from '@angular/core';
import { AppState } from '../../app.module';
import { DestinoViaje } from '../../models/destino-viaje .model';
import { VoteUpAction, VoteDownAction, VoteResetAction } from '../../models/destinos-viajes-states.model';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css']
})
export class DestinoViajeComponent implements OnInit {
  @Input() destino:DestinoViaje;
  @Input() position:number;

  /*esto accede a la clase del tag creado por angular(destino) y le agrega el col para que no se rompa el row*/
  @HostBinding('attr.class') cssClass = 'col-md-4 mb-3';
  @Output() clicked: EventEmitter<DestinoViaje>; // este es un evento de salida de datos, el opuesto de input que es para entrada de datos

  constructor(private store: Store<AppState>) {
    //se inicializa la variable 'clicked' (evento de click), para luego invocarla en lista-destinos.html
    this.clicked = new EventEmitter();
  }
 
  ngOnInit(): void {
  }

  ir(){
    this.clicked.emit(this.destino)
    return false;
  }

  voteUp(){
    this.store.dispatch(new VoteUpAction(this.destino));
    return false
  }
  voteDown(){
    this.store.dispatch(new VoteDownAction(this.destino));
    return false
  }
  voteReset(){
    this.store.dispatch(new VoteResetAction(this.destino));
    return false
  }

}
