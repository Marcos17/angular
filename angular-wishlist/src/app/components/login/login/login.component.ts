import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  mensajeError: string;
  constructor(public authService: AuthService) {
    this.mensajeError = '';
   }


  ngOnInit(): void {
  }

  login(username:string, password: string):boolean {
    this.mensajeError= '';
    if(!this.authService.login(username, password)){
      this.mensajeError = 'Login incorrecto.';
      setTimeout(function(){
        this.mensajeError = '';
      }.bind(this),2500);
    }
    return false; // se usa return false para que cuando se haga click en el <a> no intente redirigir
  }

  logout():boolean {
    this.authService.logout();
    return false;
  }
}
